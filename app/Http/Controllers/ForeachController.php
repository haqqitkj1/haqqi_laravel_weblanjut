<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ForeachController extends Controller
{
    //
    public function indexForeach()
    {
        $users= array(['nama' => 'Muhibbuddin Al Haqqi',
            'nim' => 20515070911005],
            ['nama' => 'Johnson',
            'nim' => 2121215105515]);
        return view('foreach') ->with('users', $users);
    }
}
