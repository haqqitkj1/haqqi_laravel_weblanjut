@extends('layouts.master')

@section('title', 'Section Page')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3"> Ini adalah bagian Section 'Content', yang mengisi yield pada master blade view </h1>
        </div>
    </div>
</div>

@endsection
