@extends('layouts.master')

@section('title', 'For / Foreach Page')

@section('content')

<style type="text/css">
        #content{
                float:left;
                width: 790px;
                height: ;
                margin-left:35px;
                background-color:#ef8e38;
                border-radius:50px;
                color:black;
        }
        #content p {
                font-family:"Lucida Console";
                padding:15px;
        }
        #quot{
                width:650px;
                height:50px;
                margin-left:70px;
                background-color:none;
                font-size:25px;
                font-family:Lucida Console;
                color:rgba(45,80,80,1);
        }
	</style>

        <div id=content>
			<center>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h2 class="mt-4">For</h2>
                            @for($i = 0; $i < 5; $i++)
                            <p>The Value of i is {{ $i }}</p>
                            @endfor
                        </div>
                        <div class="col">
                            <h2 class="mt-4">Foreach</h2>
                            <table border="3">
                                <tr>
                                    <th>Nama</th>
                                    <th> | </th>
                                    <th>NIM</th>
                                </tr>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user['nama'] }}</td>
                                    <th> | </th>
                                    <td>{{ $user['nim'] }}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>

                <br><br>


            </center><br>
			<div id=quot><center>"Muhibbuddin Al Haqqi - 205150709111005"</center></div>
		</div>
@endsection
