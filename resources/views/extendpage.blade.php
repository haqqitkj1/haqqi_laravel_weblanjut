@extends('layouts.master')

@section('title', 'Extends Page')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3"> This is a child view that extends from the master blade page..</h1>
        </div>
    </div>
</div>

@endsection
