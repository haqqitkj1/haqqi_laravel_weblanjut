<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\HelloWorldController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// root
// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'HelloWorldController@index');

Route::get('/extendpage', function () {
    return view('extendpage');
});

Route::get('/sectionpage', function () {
    return view('sectionpage');
});

Route::get('/foreachpage', 'ForeachController@indexForeach');
// Route::get('/', [HelloWorldController::class, 'index']);
